# This file is part of the  module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .party import Party, PartyAttribute
from .configuration import Configuration

def register():
    Pool.register(
        Party,
        PartyAttribute,
        Configuration,
        module='party_person', type_='model')