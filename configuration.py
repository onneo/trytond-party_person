# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And, Or, If

__metaclass__ = PoolMeta
__all__ = ['Configuration']


class Configuration:
    __name__ = 'party.configuration'

    name_format = fields.Selection([
                              ('N', 'Name Surname'),
                              ('S', 'Surname, Name'),
                            ],'Format', help='Name Format', required=True)

    @staticmethod
    def default_name_format():
        return 'S'

    #@classmethod
    #@fields.depends('name_format')
    #def on_change_name_format(cls):
    #    #TODO: reformat all parties (is_person) names
    #    pass
