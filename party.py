# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import fields, DictSchemaMixin, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And, Or, If

__metaclass__ = PoolMeta
__all__ = ['Party', 'PartyAttribute']


class PartyAttribute(DictSchemaMixin, ModelSQL, ModelView):
    """
    Party Attributes
    """
    __name__ = 'party.attribute'

class Party:
    __name__ = 'party.party'

    given_name = fields.Char('Name', help='Name', required=True)
        #states={'invisible': Not(Bool(Eval('is_person'))),
        #        'required': Bool(Eval('is_person'))})

    last_name = fields.Char('Last Name', help='Last Name',
        states={'invisible': Not(Bool(Eval('is_person'))),
                'required': Bool(Eval('is_person'))})
    is_person = fields.Boolean(
        'Person', states={'invisible':True},
        help='Check if the party is a person.')

    ######### Personal datas #############

    attributes = fields.Dict('party.attribute', 'Attributes')

    """birthday = fields.Date('Birthday', help='Birthday Date')
    gender = fields.Selection([
                                  ('M', 'Male'),
                                  ('F', 'Female'),
                                  ('O', 'Other')
                                ],'Gender', help='Gender')
    marital_status = fields.Selection([
                                      ('S', 'Single'),
                                      ('M', 'Married'),
                                      ('O', 'Other')
                                    ],'Marital Status', help='Marital Status')


    """
    #@classmethod
    #def __setup__(cls):
        # TODO: estudiar bien esto a ver si hay que llamar o no al padre
        #cls.name.states['invisible'] = Eval('is_person')

    @fields.depends('given_name','last_name')
    def on_change_with_name(self):
        Configuration = Pool().get('party.configuration')
        config = Configuration(1)
        if config.name_format == 'S' and self.last_name:
            self.name = self.last_name + ', ' + self.given_name
        else:
            self.name = self.given_name+ ' ' + self.last_name
        return self.name

    """
    def get_rec_name(self, name):
        if self.lastname:
            return self.lastname + ', ' + self.name
        else:
            return self.name
    """


    @classmethod
    def view_attributes(cls):
        """
        ('/form//field[@name="name"]', 'states', {
                    'invisible': Eval('is_person'),
                    }),
        """
        return [
                ('//page[@id="party_personal_data"]', 'states', {
                    'invisible': ~Eval('is_person'),
                    }),
                ]
